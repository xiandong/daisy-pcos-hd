# About this System

The project was developed by: Xian Dong (935666).  
For 25pt Computing Project of University of Melbourne.

This project was created using Swift4.2 in Xcode10.2.1.

## Third-Party Library

This project used four 3rd part libraries.  
1. DLRadioButton 1.4  
2. Charts 3.3.0  
3. M13Checkbox 3.4.0  
4. SwiftyJSON 5.0.0  
All the required packages are already listed in the pod file.

## How to use the System  

1. pod install  
2. pod update  
3. run DAISY PCOS HD.xcworkspace

If the package version is different from the version used in this repository, some code modification may be required before successfully build this application.  

## Videos
I prepared a YouTube video to demonstrate the functions of the application.  
[Video: DAISY PCOS HD Demonstration](https://youtu.be/pOrotTvz39E)